class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :title
      t.string :manufacturer
      t.decimal :price
      t.integer :category_id, foreign_key: true
      t.timestamps
    end
  end
end
