# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
	
	auto = Category.create(title: "Авто")
	auto_cosmetic = Category.create(title: "Автокосметика", parent: auto)
	Category.create([{title: "Автошампуни", parent: auto_cosmetic}, {title: "Автоэмали", parent: auto_cosmetic},
	{title: "Грунтовка", parent: auto_cosmetic}])
	auto_chemical = Category.create(title: "Автохимия", parent: auto)
	Category.create([{title: "Антикоры", parent: auto_chemical}, {title: "Герметики", parent: auto}, {title: "Жидкости для ГУР", parent: auto}])
	auto_acces = Category.create(title: "Аксессуары", parent: auto)
	auto_acces_def = Category.create(title: "Защита", parent: auto_acces)
	Category.create([{title: "Дефлекторы", parent: auto_acces_def}, {title: "Защита фар", parent: auto_acces_def}])
	computer = Category.create(title: "Компьютеры")
	comp_acces = Category.create(title: "Аксессуары и расходные материалы", parent: computer)
	Category.create([{title: "USB-концентраторы", parent: comp_acces}, {title: "Аккумуляторы для ноутбуков", parent: comp_acces}, {title: "Инструменты", parent: comp_acces}])
	computer_compl = Category.create(title: "Комплектующие", parent: computer)
	Category.create([{title: "Корпуса", parent: computer_compl}, {title: "Звуковые карты", parent: computer_compl}])

	Category.all.each do |category| 
		10.times do
			random_title = (0...8).map { (65 + rand(26)).chr }.join
			random_manufacturer = (0...8).map { (65 + rand(26)).chr }.join
			random_price = rand(0.1..99.9)
			Product.create(title: random_title, manufacturer: random_manufacturer,
			price: random_price, category_id: category.id)
		end
	end
