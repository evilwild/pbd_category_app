Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'application#index'
  get 'show_products/:id', to: "application#show_products", as: 'show_products'
  resources :category, only: %i[create new delete destroy edit update patch]
  resources :product, only: %i[create new delete destroy edit update patch]
end
