class CategoryController < ApplicationController

  def create
    @category = Category.create(user_params)
    if @category.save
      redirect_to root_path
    else
      render :new
    end
  end

  def destroy
    Category.find(params[:id]).destroy
    redirect_to root_path
  end

  def new
    @category = Category.new
  end

  def edit
    @category = Category.find(params[:id])
  end

  def update
    @category = Category.find(params[:id])
    if @category.update_attributes(user_params)
      redirect_to root_path
    else
      render :edit
    end
  end

  def user_params
    params.require(:category).permit(:title, :parent_id)
  end

end
