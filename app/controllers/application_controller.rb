class ApplicationController < ActionController::Base
  def index
    @products = Product.all
    @categories = Category.all
    render 'index'
  end

  def show_products
    @products = Product.where(:category_id => Category.find(params[:id]).subtree_ids)
    @categories = Category.all
    render 'index'
  end
end
